let lastResult = 0;

function updateDisplay() {
    document.getElementById('box').innerText = currentInput;
}

function button_number(number) {
    currentInput += number;
    updateDisplay();
}

function button_clear() {
    currentInput = '';
    lastResult = 0;
    lastOperation = '';
    updateDisplay();
}
function clear_entry() {
    currentInput = '';
    updateDisplay();
}

function backspace_remove() {
    currentInput = currentInput.slice(0, -1);
    updateDisplay();
}

function calculate_percentage() {
    const percentage = parseFloat(currentInput) / 100;
    currentInput = percentage.toString();
    updateDisplay();
}

function division_one() {
    const number = parseFloat(currentInput);
    if (number !== 0) {
        currentInput = (1 / number).toString();
        updateDisplay();
    } else {
        alert("Cannot divide by zero!");
    }
}

function power_of() {
    const number = parseFloat(currentInput);
    currentInput = (number ** 2).toString();
    updateDisplay();
}

function square_root() {
    const number = parseFloat(currentInput);
    if (number >= 0) {
        currentInput = Math.sqrt(number).toString();
        updateDisplay();
    } else {
        alert("Cannot calculate square root of a negative number!");
    }
}

function plus_minus() {
    const number = parseFloat(currentInput);
    currentInput = (-number).toString();
    updateDisplay();
}

function performOperation() {
    const currentNumber = parseFloat(currentInput);

    switch (lastOperation) {
        case '+':
            lastResult += currentNumber;
            break;
        case '-':
            lastResult -= currentNumber;
            break;
        case '*':
            lastResult *= currentNumber;
            break;
        case '/':
            if (currentNumber !== 0) {
                lastResult /= currentNumber;
            } else {
                alert("Cannot divide by zero!");
            }
            break;
        default:
            lastResult = currentNumber;
            break;
    }

    currentInput = '';
    lastOperation = '';
    updateDisplay();
}

function button_equal() {
    performOperation();
    currentInput = lastResult.toString();
    updateDisplay();
}